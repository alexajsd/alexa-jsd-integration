package com.atlassian.jsd;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CreateTicket {

    private JSDInstance jsdInstance = new JSDInstance();

    public String createTicket(String summary, String description) {
        try {
            ClientConfig clientConfig = new ClientConfig();
            clientConfig.register(basicAuthentication());

            Client client = ClientBuilder.newClient(clientConfig);
            String url = jsdInstance.getRestBaseUrl() + "/rest/servicedeskapi/request";
            System.out.println("Hitting URL  " + url);

            WebTarget webTarget = client.target(url);
            Response response = webTarget
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .accept(MediaType.APPLICATION_JSON)
                    .post(Entity.json(createRequest(summary, description)));
            String result = response.readEntity(String.class);
            System.out.println("The result " + result);
            JSONObject object = null;
            try {
                object = new JSONObject(result);
                return object.getString("issueKey");
            } catch (JSONException e) {
                System.out.println("Problem in fetching issue key " + e.getMessage());
                throw new RuntimeException(e);
            }
        } catch(Exception ex) {
            System.out.println("Huge problem guys  " + ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    private String createRequest(String summary, String description) {
        return "{\n" +
                "    \"serviceDeskId\": \"2\",\n" +
                "    \"requestTypeId\": \"15\",\n" +
                "    \"requestFieldValues\": {\n" +
                "        \"summary\": \"" + summary + "\",\n" +
                "        \"description\": \"" + description + "\"\n" +
                "    }\n" +
                "}";
    }

    private HttpAuthenticationFeature basicAuthentication() {
        return HttpAuthenticationFeature.basic(jsdInstance.getUserName(), jsdInstance.getPassword());
    }

    public static void main(String[] args) {
        CreateTicket ct = new CreateTicket();
        String issue = ct.createTicket("Bkha","chal ja mere baap");
        System.out.println(issue);
    }

}
