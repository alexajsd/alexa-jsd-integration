package com.atlassian.jsd.model;

public class IssueStatusAndLastComment {
    private String status;
    private String comment;

    public IssueStatusAndLastComment(String status, String comment) {
        this.status = status;
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "IssueStatusAndLastComment{" +
                "status='" + status + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
