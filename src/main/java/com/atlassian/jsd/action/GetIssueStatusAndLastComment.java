package com.atlassian.jsd.action;

import com.atlassian.jsd.JSDInstance;
import com.atlassian.jsd.model.IssueStatusAndLastComment;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class GetIssueStatusAndLastComment {

    private static JSDInstance jsdInstance = new JSDInstance();
    private HttpClient client = HttpClientBuilder.create().build();

    public IssueStatusAndLastComment execute(String issueKey) throws IOException {
        HttpGet request = new HttpGet(requestUrl(issueKey));
        request.setHeader(HttpHeaders.AUTHORIZATION, auth());
        HttpResponse response = client.execute(request);

        String content = IOUtils.toString(response.getEntity().getContent());
        Gson gson = new Gson();
        JsonObject issueJson = gson.fromJson(content, JsonObject.class);
        JsonObject commentObject = issueJson.getAsJsonObject("fields").getAsJsonObject("comment");
        String status = issueJson.getAsJsonObject("fields").getAsJsonObject("status").get("name").getAsString();
        String lastComment = null;
        if (commentObject != null) {
            JsonArray commentsArray = commentObject.getAsJsonArray("comments");
            if (commentsArray != null && commentsArray.size() > 0) {
                lastComment = commentsArray.get(commentsArray.size() -1 ).getAsJsonObject().get("body").getAsString();
            }
        }

        return new IssueStatusAndLastComment(status, lastComment);
    }

    private String requestUrl(String issueKey) {
        return String.format("%s/rest/api/2/issue/%s", jsdInstance.getRestBaseUrl(), issueKey);
    }

    private static String auth() {
        String auth = jsdInstance.getUserName() + ":" + jsdInstance.getPassword();
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(StandardCharsets.ISO_8859_1));
        return "Basic " + new String(encodedAuth);

    }
}
