package com.atlassian;

import com.amazon.speech.json.SpeechletRequestEnvelope;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.speechlet.SpeechletV2;
import com.amazon.speech.ui.OutputSpeech;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.SimpleCard;
import com.amazon.speech.ui.SsmlOutputSpeech;
import com.atlassian.jsd.CreateTicket;
import com.atlassian.jsd.action.GetIssueStatusAndLastComment;
import com.atlassian.jsd.model.IssueStatusAndLastComment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jsd.JSDInstance.PROJECT_KEY;

/**
 * This sample shows how to create a simple speechlet for handling speechlet requests.
 */
public class EntryPointSpeechlet implements SpeechletV2 {
    private static final Logger log = LoggerFactory.getLogger(EntryPointSpeechlet.class);

    private Map<String, String> requestProblemMap = new HashMap<>();

    @Override
    public void onSessionStarted(SpeechletRequestEnvelope<SessionStartedRequest> requestEnvelope) {
        log.info("onSessionStarted requestId={}, sessionId={}", requestEnvelope.getRequest().getRequestId(),
                requestEnvelope.getSession().getSessionId());
        // any initialization logic goes here
    }

    @Override
    public SpeechletResponse onLaunch(SpeechletRequestEnvelope<LaunchRequest> requestEnvelope) {
        System.out.println("onLaunch ............. " + requestEnvelope.getSession().getSessionId());

        log.info("onLaunch requestId={}, sessionId={}", requestEnvelope.getRequest().getRequestId(),
                requestEnvelope.getSession().getSessionId());
        return getWelcomeResponse();
    }

    @Override
    public SpeechletResponse onIntent(SpeechletRequestEnvelope<IntentRequest> requestEnvelope) {
        IntentRequest request = requestEnvelope.getRequest();
        System.out.println("onIntent requestId={}, sessionId=" + request.getRequestId());

        Intent intent = request.getIntent();
        String intentName = (intent != null) ? intent.getName() : "";
        System.out.println("intentName ............. " + intentName);

        if (intent != null) {
            System.out.println("intent slots ............. " + intent.getSlots());
            System.out.println("request ............. " + requestEnvelope.getRequest().toString());

            intent.getSlots().entrySet()
                    .stream()
                    .map(entry ->
                            String.join(":", entry.getKey(), entry.getValue().getValue()))
                    .forEach(System.out::println);
        }

        switch (intentName) {
            case "problem":
                return saveProblemAndAskForMoreInformation(requestEnvelope.getSession(), intent.getSlot("all").getValue());
            case "context":
                return createTicketAndRespond(requestEnvelope.getSession(), intent.getSlot("device_number").getValue());
            case "status":
                return getTicketStatusAndRespond(intent.getSlots().get("ticket_number").getValue());
            default:
                return getWelcomeResponse();
        }
    }

    private SpeechletResponse createTicketAndRespond(Session session, String deviceName) {
        String description = requestProblemMap.get(session.getSessionId());
        CreateTicket createTicket = new CreateTicket();
        String ticket = createTicket.createTicket(description + ". Device - " + deviceName, "Request raised from Alexa for device " + deviceName);
        System.out.println("Ticket id created is " + ticket);
        String speechText = "<speak>I have created a ticket for you the ticket id is <say-as interpret-as=\"spell-out\">" + ticket.replace(PROJECT_KEY + "-", "") + "</say-as></speak>";

        // Create the Simple card content.
        SimpleCard card = getSimpleCard("Ticket", speechText);

        // Create the plain text output.
        SsmlOutputSpeech ssmlOutputSpeech = new SsmlOutputSpeech();
        ssmlOutputSpeech.setSsml(speechText);
        //PlainTextOutputSpeech speech = getPlainTextOutputSpeech(speechText);

        return SpeechletResponse.newTellResponse(ssmlOutputSpeech, card);
    }

    private SpeechletResponse getTicketStatusAndRespond(String ticketId) {
        String speechText = "";
        try {
            IssueStatusAndLastComment issue = new GetIssueStatusAndLastComment().execute(String.format("%s-%s", PROJECT_KEY, ticketId));

            if (issue.getComment() != null) {
                speechText = String.format("<speak>Ticket status is <emphasis level=\"moderate\">%s</emphasis>. And the last comment was %s</speak>", issue.getStatus(), issue.getComment());
            } else {
                speechText = String.format("<speak>Ticket status is %s. No comments were added.</speak>", issue.getStatus());
            }
        } catch (IOException e) {
            e.printStackTrace();
            speechText = "<speak>I was not able to fetch the status of your ticket</speak>";
        }
        // Create the Simple card content.
        SimpleCard card = getSimpleCard("Ticket", speechText);

        SsmlOutputSpeech ssmlOutputSpeech = new SsmlOutputSpeech();
        ssmlOutputSpeech.setSsml(speechText);

        return SpeechletResponse.newTellResponse(ssmlOutputSpeech, card);
    }

    private SpeechletResponse saveProblemAndAskForMoreInformation(Session session, String problemDescription) {
        requestProblemMap.put(session.getSessionId(), problemDescription);

        String speechText = "What is your device number, you can find your device number at the back of your device.";

        // Create the Simple card content.
        SimpleCard card = getSimpleCard("Ticket", speechText);

        // Create the plain text output.
        //PlainTextOutputSpeech speech = getPlainTextOutputSpeech(speechText);

        return getAskResponse("Device Number", speechText);
    }

    @Override
    public void onSessionEnded(SpeechletRequestEnvelope<SessionEndedRequest> requestEnvelope) {
        log.info("onSessionEnded requestId={}, sessionId={}", requestEnvelope.getRequest().getRequestId(),
                requestEnvelope.getSession().getSessionId());
        // any cleanup logic goes here
    }

    /**
     * Creates and returns a {@code SpeechletResponse} with a welcome message.
     *
     * @return SpeechletResponse spoken and visual response for the given intent
     */
    private SpeechletResponse getWelcomeResponse() {
        String speechText = "Welcome to Help Desk. How may I help you?";
        return getAskResponse("HelloWorld", speechText);
    }

    /**
     * Helper method that creates a card object.
     *
     * @param title   title of the card
     * @param content body of the card
     * @return SimpleCard the display card to be sent along with the voice response.
     */
    private SimpleCard getSimpleCard(String title, String content) {
        SimpleCard card = new SimpleCard();
        card.setTitle(title);
        card.setContent(content);

        return card;
    }

    /**
     * Helper method for retrieving an OutputSpeech object when given a string of TTS.
     *
     * @param speechText the text that should be spoken out to the user.
     * @return an instance of SpeechOutput.
     */
    private PlainTextOutputSpeech getPlainTextOutputSpeech(String speechText) {
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        return speech;
    }

    /**
     * Helper method that returns a reprompt object. This is used in Ask responses where you want
     * the user to be able to respond to your speech.
     *
     * @param outputSpeech The OutputSpeech object that will be said once and repeated if necessary.
     * @return Reprompt instance.
     */
    private Reprompt getReprompt(OutputSpeech outputSpeech) {
        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(outputSpeech);

        return reprompt;
    }

    /**
     * Helper method for retrieving an Ask response with a simple card and reprompt included.
     *
     * @param cardTitle  Title of the card that you want displayed.
     * @param speechText speech text that will be spoken to the user.
     * @return the resulting card and speech text.
     */
    private SpeechletResponse getAskResponse(String cardTitle, String speechText) {
        SimpleCard card = getSimpleCard(cardTitle, speechText);
        PlainTextOutputSpeech speech = getPlainTextOutputSpeech(speechText);
        Reprompt reprompt = getReprompt(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt, card);
    }
}