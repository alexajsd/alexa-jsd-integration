package com.atlassian;

import com.amazon.speech.speechlet.servlet.SpeechletServlet;

public class EntryPointServlet extends SpeechletServlet {

    public EntryPointServlet() {
        this.setSpeechlet(new EntryPointSpeechlet());
    }
}
