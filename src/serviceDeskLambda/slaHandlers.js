const httpUtils = require('./httpUtils.js');

module.exports = {
    'sla_intent': function() {
        const respond = speechOutput => {
            this.attributes.speechOutput = speechOutput;
            this.response.speak(speechOutput);
            this.emit(':responseReady');
        };

        httpUtils.httpGet('http://54.252.247.145:8080/rest/servicedeskapi/servicedesk/2/queue/24/issue', response => {
            const queuesResponse = JSON.parse(response);
            const issues = queuesResponse.values;

            if (issues && issues.length > 0) {
                respond(`S L A is breached for ${issues.length} tickets. Agents cannot go home. I'm locking the doors`);
            } else {
                respond(`It's a great day today. We have 0 tickets with breached S L A. Agents can go home. Unlocking the doors.`);
            }

        });
    }
};
