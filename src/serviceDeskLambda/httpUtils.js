const http = require('http');

function sendHttpRequest(method, pathAndQuery, callback, requestBody) {
    const options = {
        host: '54.252.247.145',
        port: '8080',
        path: pathAndQuery,
        method: method,
        headers: {
            Authorization: 'Basic YWRtaW46TWFzdGVyUGFzc3dvcmQxOA==',
            'X-ExperimentalApi': 'opt-in',
            'Content-Type': 'application/json'
        }
    };

    const req = http.request(options, (res) => {
        res.setEncoding('utf8');
        let rawData = '';

        //accept incoming data asynchronously
        res.on('data', chunk => {
            rawData = rawData + chunk;
        });

        //return the data when streaming is complete
        res.on('end', () => {
            console.log(rawData);
            callback(rawData);
        });

    });
    if (requestBody) {
        console.log('requestBody: ' + JSON.stringify(requestBody));
        req.write(JSON.stringify(requestBody));
    }
    req.end();
}

const httpGet = sendHttpRequest.bind(null, 'GET');
const httpPost = sendHttpRequest.bind(null, 'POST');

module.exports = {
    httpGet,
    httpPost
};
