module.exports = {
    'yes': function() {

        const openTextFromSlot = this.event.request.intent.slots.openText.value.toLowerCase();

        var speechOutput = 'YES NOT FOUND WHATEVER DUDES';

        if(this.attributes.ticketReference) {
            speechOutput = 'Adding comment ' + openTextFromSlot + ' to ticket reference ' + this.attributes.ticketReference;
        }

        this.attributes.speechOutput = speechOutput;

        this.response.speak(speechOutput);
        this.emit(':responseReady');
    }
};
