var http = require('http');

function httpGet(pathAndQuery, callback) {
    var options = {
        host: '54.252.247.145',
        port: '8080',
        path: pathAndQuery,
        method: 'GET',
        headers: {
            Authorization: 'Basic YWRtaW46TWFzdGVyUGFzc3dvcmQxOA==',
            'X-ExperimentalApi': 'opt-in'
        }
    };

    var req = http.request(options, function(res) {
        res.setEncoding('utf8');
        var rawData = '';

        //accept incoming data asynchronously
        res.on('data', function(chunk) {
            rawData = rawData + chunk;
        });

        //return the data when streaming is complete
        res.on('end', function() {
            console.log(rawData);
            callback(rawData);
        });

    });
    req.end();
}

module.exports = {
    'list_queues': function() {
        var that = this;

        httpGet('/rest/servicedeskapi/servicedesk/2/queue', function(response) {
            var queuesResponse = JSON.parse(response);
            var myQueues = queuesResponse.values;

            var speechOutput = 'There are ' + myQueues.length + ' queue. They are';

            if(myQueues.length == 0) {
                speechOutput = 'You have no queues';
            }
            else if (myQueues.length == 1) {
                speechOutput = 'You have one queue, its called ' + myQueues[0].name;
            }
            else {
                for (var i = 0; i < myQueues.length; i++) {
                    if(i == 0) {
                        console.log(myQueues[i]);
                        speechOutput = speechOutput + '' + myQueues[i].name;
                    }
                    if (i > 0 && i  < myQueues.length - 1 || myQueues.length == 1) {
                        console.log(myQueues[i]);
                        speechOutput = speechOutput + ',' + myQueues[i].name;
                    }
                    else if (i == myQueues.length - 1) {
                        console.log(myQueues[i]);
                        speechOutput = speechOutput + ' and ' + myQueues[i].name;
                    }
                }
            }

            that.attributes.speechOutput = speechOutput;
            that.response.speak(speechOutput);
            that.emit(':responseReady');
        });
    }
};
