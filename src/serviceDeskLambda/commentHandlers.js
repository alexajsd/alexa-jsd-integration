const httpUtils = require('./httpUtils.js');

module.exports = {
    'add_comment': function() {
        const respond = speechOutput => {
            this.attributes.speechOutput = speechOutput;
            this.response.speak(speechOutput);
            this.emit(':responseReady');
        };

        const ticketReference = this.attributes.ticketReference;
        if (!ticketReference) {
            respond('Please transition a ticket first before adding a comment.');
            return;
        }
        const addCommentPath = `/rest/servicedeskapi/request/HAR-${ticketReference}/comment`;
        const comment = this.event.request.intent.slots.comment.value.toLowerCase();
        console.log(`addCommentPath: ${addCommentPath}`);
        console.log(`comment: ${comment}`);

        httpUtils.httpPost(
            addCommentPath,
            response => {
                console.log('addComment response: ' + JSON.stringify(response));
                respond(`Comment added to ticket ${ticketReference}.`);
            },
            {
                "body": comment,
                "public": true
            }
        );
    }
};
