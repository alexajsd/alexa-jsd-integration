module.exports = {
    'queue_next': function() {

        const itemSlot = this.event.request.intent.slots.Item;
        let itemName;
        if (itemSlot && itemSlot.value) {
            itemName = itemSlot.value.toLowerCase();
        }

        var responseSpeech = '';

        if(itemName == 'Laptops') {
            responseSpeech = 'NEXT: There are 4 issues under the ' + itemName + ' queue. You are under SLA on this queue.';
        }

        if(itemName == 'Passwords') {
            responseSpeech = 'NEXT: There are 1432 issues under the ' + itemName + ' queue. You are over SLA on this queue.';
        }

        else {
            responseSpeech = 'NEXT: The ' + itemName + ' queue does not exist, please pick another one. Or ask me which queues you are working on today and I can help.';
        }

        this.attributes.speechOutput = responseSpeech;

        this.response.speak(responseSpeech);
        this.emit(':responseReady');
    }
};
