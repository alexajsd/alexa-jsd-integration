const httpUtils = require('./httpUtils');

module.exports = {
    'queue_summary': function() {
        const queueNameFromSlot = this.event.request.intent.slots.queue_name.value.toLowerCase();
        let speechOutput = `Queue name is ${queueNameFromSlot}`;

        const respond = speechOutput => {
            this.attributes.speechOutput = speechOutput;
            this.response.speak(speechOutput);
            this.emit(':responseReady');
        };

        httpUtils.httpGet('/rest/servicedeskapi/servicedesk/2/queue', response => {
            const queuesResponse = JSON.parse(response);
            const matchedQueues = queuesResponse.values.filter(queue => {
                return queue.name.toLowerCase() === queueNameFromSlot;
            });
            if (matchedQueues.length === 0) {
                respond(`Sorry, but queue ${queueNameFromSlot} cannot be found.`);
                return;
            } else if (matchedQueues.length > 1) {
                respond(`Sorry, but there are multiple queues matching ${queueNameFromSlot}.`);
                return;
            }

            const matchedQueue = matchedQueues[0];
            // SLA field: `values[0].fields.customfield_10202`

            const path = `/rest/servicedeskapi/servicedesk/2/queue/${matchedQueue.id}/issue`;
            console.log('Path is: ' + path);
            httpUtils.httpGet(path, response => {
                const issues = JSON.parse(response).values;
                let firstIssueDescription = '';
                if (issues.length > 0) {
                    firstIssueDescription = `The top issue says ${issues[0].fields.summary}`
                }
                respond(`There ${issues.length > 1 ? 'are' : 'is'} ${issues.length} issue${issues.length > 1 ? 's' : ''} in this queue. ${firstIssueDescription}`);
            });
        });
    }
};
