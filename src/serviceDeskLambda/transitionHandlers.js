const httpUtils = require('./httpUtils.js');

const statusMap = {
    'in progress': '891',
    'resolved': '761'
};

module.exports = {
    'transition_ticket': function() {
        const ticketReferenceFromSlot = this.event.request.intent.slots.ticket_reference.value.toLowerCase();
        const issueStatusFromSlot = this.event.request.intent.slots.status.value.toLowerCase();

        const respond = speechOutput => {
            this.attributes.speechOutput = speechOutput;
            this.response.speak(speechOutput).listen('');
            this.emit(':responseReady');
        };

        const statusCode = statusMap[issueStatusFromSlot];
        console.log('statusCode: ' + statusCode);

        this.attributes.ticketReference = ticketReferenceFromSlot;
        if (!statusCode) {
            respond(`Transition to status ${issueStatusFromSlot} is not supported.`);
            return;
        }
        const postPath = `/rest/api/2/issue/HAR-${ticketReferenceFromSlot}/transitions`;
        console.log('postPath: ' + postPath);
        httpUtils.httpPost(
            postPath,
            response => {
                respond(`Ticket ${ticketReferenceFromSlot} is transitioned to ${issueStatusFromSlot}`);
            },
            {
                "transition": {
                    "id": statusCode
                }
            }
        );
    }
};
